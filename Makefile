.PHONY:deps
.PHONY:clean
.PHONY:build
.PHONY:run
.PHONY:ship

deps:
	go get -u ./...

clean: 
	rm -rf app

build:
	GOOS=linux GOARCH=amd64 go build -o app ./hello-world

run: 	build
	sam local invoke app -e event.json

ship:	build
	zip app.zip app
